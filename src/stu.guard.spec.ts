import { TestBed } from '@angular/core/testing';

import { StuGuard } from './stu.guard';

describe('StuGuard', () => {
  let guard: StuGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(StuGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
