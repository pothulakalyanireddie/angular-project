import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CareerprofessionalsComponent } from './careerprofessionals/careerprofessionals.component';
import { CareertestComponent } from './careertest/careertest.component';
import { ChildComponent } from './child/child.component';
import { FindcounselorsComponent } from './findcounselors/findcounselors.component';
import { InstitutionsComponent } from './institutions/institutions.component';
import { LoginComponent } from './login/login.component';
import { ParentComponent } from './parent/parent.component';
import { PartnerComponent } from './partner/partner.component';
import { RoutequareyviewComponent } from './routequareyview/routequareyview.component';
import { RoutequerydataComponent } from './routequerydata/routequerydata.component';
import { StudentComponent } from './student/student.component';
const routes: Routes = [{path:'student',component:StudentComponent},
{path:'app-root',component:AppComponent},
{path:'institutions',component:InstitutionsComponent},
{path:'careerprofessionals',component:CareerprofessionalsComponent},
{path:'careertest',component:CareertestComponent},
{path:'login',component:LoginComponent},
{path:'findcounselors',component:FindcounselorsComponent},
{path:'parent',component:ParentComponent},
{path:'routequerydata',component:RoutequerydataComponent},
{path:'routequeryview',component:RoutequareyviewComponent},
{path:'child',component:ChildComponent},
{path:'partner',component:PartnerComponent},
{path:'',redirectTo:'/student',pathMatch:'full'}
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
