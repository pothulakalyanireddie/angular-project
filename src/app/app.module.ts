import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StudentComponent } from './student/student.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MainheaderComponent } from './mainheader/mainheader.component';
import { InstitutionsComponent } from './institutions/institutions.component';
import { FindcounselorsComponent } from './findcounselors/findcounselors.component';
import { CareerprofessionalsComponent } from './careerprofessionals/careerprofessionals.component';
import { LoginComponent } from './login/login.component';
import { CareertestComponent } from './careertest/careertest.component';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import { FormsModule } from '@angular/forms'
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import{FindcounselorsserviceService} from './findcounselors/findcounselorsservice.service';
import {MatDatepickerModule} from '@angular/material/datepicker';
import{MatMomentDateModule} from '@angular/material-moment-adapter';
import { ToastrModule } from 'ngx-toastr';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import{studentpipe} from './student/studentpipe';
import { RoutequerydataComponent } from './routequerydata/routequerydata.component';
import { RoutequareyviewComponent } from './routequareyview/routequareyview.component';
import { PartnerComponent } from './partner/partner.component'
@NgModule({
  declarations: [
    AppComponent,
    StudentComponent,
    MainheaderComponent,
    InstitutionsComponent,
    FindcounselorsComponent,
    CareerprofessionalsComponent,
    LoginComponent,
    CareertestComponent,
    ParentComponent,
    ChildComponent,
    studentpipe,
    RoutequerydataComponent,
    RoutequareyviewComponent,
    PartnerComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    HttpClientModule,
    MatDatepickerModule,MatMomentDateModule,
    ToastrModule.forRoot({
      autoDismiss: true,
      closeButton: true
      // disableTimeOut: true
    }),
  

     ],
  providers: [FindcounselorsserviceService,MatDatepickerModule],
  bootstrap: [AppComponent],
})
export class AppModule { 

}
