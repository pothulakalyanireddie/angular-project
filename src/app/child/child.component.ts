import { Component, OnInit,Input,Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
child: string='child'
@Output() childvar:EventEmitter<string>=new EventEmitter<string>()
  constructor() { }

  ngOnInit(): void {
  }
  changetochild()
{
  this.childvar.emit(this.child)

}
}