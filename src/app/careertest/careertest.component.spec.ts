import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CareertestComponent } from './careertest.component';

describe('CareertestComponent', () => {
  let component: CareertestComponent;
  let fixture: ComponentFixture<CareertestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CareertestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CareertestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
