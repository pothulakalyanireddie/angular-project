import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-careertest',
  templateUrl: './careertest.component.html',
  styleUrls: ['./careertest.component.css']
})
export class CareertestComponent implements OnInit {

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
  }
  fileName = '';


  onFileSelected(event:any) {

      const file:File = event.target.files[0];

      if (file) {

          this.fileName = file.name;

          const formData = new FormData();

          formData.append("thumbnail", file);

          const upload$ = this.http.post("/api/thumbnail-upload", formData);

          upload$.subscribe();
      }
  }
}

