import { query } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-routequerydata',
  templateUrl: './routequerydata.component.html',
  styleUrls: ['./routequerydata.component.css']
})
export class RoutequerydataComponent implements OnInit {
  object: object = [];
  completeform: any
  emailModel:any
  fName:any;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.completeform = new FormGroup({
      fullname: new FormControl('', [Validators.required, Validators.maxLength(2)]),
      email: new FormControl('', [Validators.required, Validators.email]),
    })
  }
  formdata(fName:any , email: any) {
   var object  = {
      "fname": fName,
      "email": email
   }
    console.log(object)
    this.router.navigate(['/routequeryview'], { queryParams: { data: JSON.stringify(object) } })

  }

}
