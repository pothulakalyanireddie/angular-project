import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutequerydataComponent } from './routequerydata.component';

describe('RoutequerydataComponent', () => {
  let component: RoutequerydataComponent;
  let fixture: ComponentFixture<RoutequerydataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutequerydataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutequerydataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
