import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerprofessionalsComponent } from './careerprofessionals.component';

describe('CareerprofessionalsComponent', () => {
  let component: CareerprofessionalsComponent;
  let fixture: ComponentFixture<CareerprofessionalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CareerprofessionalsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerprofessionalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
