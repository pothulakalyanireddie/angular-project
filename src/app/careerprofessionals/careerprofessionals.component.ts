import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import{FindcounselorsserviceService} from'../findcounselors/findcounselorsservice.service';

@Component({
  selector: 'app-careerprofessionals',
  templateUrl: './careerprofessionals.component.html',
  styleUrls: ['./careerprofessionals.component.css']
})
export class CareerprofessionalsComponent implements OnInit {

  constructor(private service:FindcounselorsserviceService) { }
  public sampleform :any
  startDate=new Date()
  // dateerror="enter valid date"
  // datecon:boolean=true
  fname:any;email:any;contact:any;
  todate:any;fromdate:any
  
ngOnInit(): void {
this.sampleform =new FormGroup({
    smname:new FormControl('',[Validators.required]),
    smemail:new FormControl('',[Validators.required,Validators.email]),
    smcontact:new FormControl('',[Validators.required,Validators.maxLength(10),Validators.minLength(10)]),
    smdate:new FormControl('',[Validators.required]),
    smdate1:new FormControl('',[Validators.required]),
  
})

  }
// submit(){
//   if (this.sampleform.controls.smdate.value < this.sampleform.controls.smdate1.value){
//     console.log('not')
//    this.datecon=true
// //  return (this.dateerror="do enter valid date")
//   } 
//   else {
//   // return  (this.dateerror='')
//   console.log('valid')
//   this.datecon=false
// }
submit(fname:any,email:any,contact:any,todate:any,fromdate:any){

const body={fn:fname,email:email,contact:contact,todate:todate,fromdate:fromdate}
console.log( body)
this.service.postdata1(body).subscribe(data=>{console.log(data)})
}
}

