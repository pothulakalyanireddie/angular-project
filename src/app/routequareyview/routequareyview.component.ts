import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-routequareyview',
  templateUrl: './routequareyview.component.html',
  styleUrls: ['./routequareyview.component.css']
})
export class RoutequareyviewComponent implements OnInit {
  completeforms: any
  constructor(private activatedroute: ActivatedRoute) { }
  preselecteddata: any
  ngOnInit(): void {
    this.completeforms = new FormGroup({
      fullnames: new FormControl('', [Validators.required, Validators.maxLength(2)]),
      emails: new FormControl('', [Validators.required, Validators.email]),
    })
    this.activatedroute.queryParams.subscribe((params: any) => {
      this.preselecteddata = params['data'];
    })
    
    console.log(this.preselecteddata)
    console.log(this.preselecteddata.fname)
    // this.preselected();
  }
  preselected() {
    let fullnames = <FormControl>this.completeforms.get('fullnames')
    let emails = <FormControl>this.completeforms.get(' emails')
    fullnames.setValue(this.preselecteddata.fname)
    emails.setValue(this.preselecteddata.email)
  }
  // (params:any)=>console.log(params)
}
