import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutequareyviewComponent } from './routequareyview.component';

describe('RoutequareyviewComponent', () => {
  let component: RoutequareyviewComponent;
  let fixture: ComponentFixture<RoutequareyviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutequareyviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutequareyviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
