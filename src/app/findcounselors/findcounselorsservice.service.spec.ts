import { TestBed } from '@angular/core/testing';

import { FindcounselorsserviceService } from './findcounselorsservice.service';

describe('FindcounselorsserviceService', () => {
  let service: FindcounselorsserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FindcounselorsserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
