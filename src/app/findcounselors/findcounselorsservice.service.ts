import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class FindcounselorsserviceService {
  urljson1="https://jsonplaceholder.typicode.com"
  pageNumber:any
  pageSize:any
  timezone:any
  constructor(private http1:HttpClient,
    private toastr: ToastrService) { 
      
    }
 
  getjson1(Id:any){
    const params = new HttpParams().set('pageNumber', this.pageNumber).set('pageSize', this.pageSize).set('timezone', this.timezone);
 return this.http1.get( this.urljson1+'/posts')
  }
  showSuccess(message:any, title:any){
    this.toastr.success(message, title)
}

showError(message:any, title:any){
    this.toastr.error(message, title)
}

showInfo(message:any, title:any){
    this.toastr.info(message, title)
}

showWarning(message:any, title:any){
    this.toastr.warning(message, title)
}
postdata1(body1:any){ 
  const body=JSON.stringify(body1)
  return this.http1.post(this.urljson1+'/postdata1',body)
}

}
function pageNumber(arg0: string, pageNumber: any) {
  throw new Error('Function not implemented.');
}

function pageSize(arg0: string, pageSize: any) {
  throw new Error('Function not implemented.');
}

function timezone(arg0: string, timezone: any) {
  throw new Error('Function not implemented.');
}

