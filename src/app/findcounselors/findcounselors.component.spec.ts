import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindcounselorsComponent } from './findcounselors.component';

describe('FindcounselorsComponent', () => {
  let component: FindcounselorsComponent;
  let fixture: ComponentFixture<FindcounselorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FindcounselorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FindcounselorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
