import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validator, Validators } from '@angular/forms';
import { FindcounselorsserviceService } from './findcounselorsservice.service';
import { InstitutionsComponent } from '../institutions/institutions.component';

@Component({
  selector: 'app-findcounselors',
  templateUrl: './findcounselors.component.html',
  styleUrls: ['./findcounselors.component.css']
})
export class FindcounselorsComponent implements OnInit {
  @ViewChild(InstitutionsComponent, {static: true})  highlightedText: InstitutionsComponent | any;
  public completeform: any
  constructor(private apiservice: FindcounselorsserviceService) { }
  tostevar: any
  fn: string = ''
  inputMale = 'Manoj Reddy';
  male1: boolean = true;
  female: boolean = false;
  others: boolean = false;
  parentInput= 'Manoj Reddy'
Iddata:number=1;
  ngOnInit(): void {
    this.completeform = new FormGroup({
      fullname: new FormControl('', [Validators.required, Validators.maxLength(2)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      gender: new FormControl('', [Validators.required]),
      maritalstatus: new FormControl(''),
    })
    this.getjson1api();
  }
  fninput() {
    this.fn = this.completeform.get('fullname').value
    return this.fn
  }

  showToasterSuccess() {
    this.apiservice.showSuccess("Data shown successfully !!", "ItSolutionStuff.com")
  }

  showToasterError() {
    this.apiservice.showError("Something is wrong", "ItSolutionStuff.com")
  }

  showToasterInfo() {
    this.apiservice.showInfo("This is info", "ItSolutionStuff.com")
  }

  showToasterWarning() {
    this.apiservice.showWarning("This is warning", "ItSolutionStuff.com")

  }
  showToastervar(tostevar: any) {
    if (tostevar)
      this.apiservice.showSuccess(tostevar, "ItSolutionStuff.com")
  }
  getjson1api() {
    
    this.apiservice.getjson1(this.Iddata).subscribe(data => { console.log(data) })
  }


  emailerror(): any {

    if (this.completeform.get('email').hasError('required')) {
      console.log('req')
      return 'You must enter a value'
    }
    if (this.completeform.get('email').hasError('email')) {
      console.log('notvalid')
      return 'enter valid email'
    }
  }

  //  emailErrMessage() {
  //   return this.account.controls.email.hasError('required') ? ' Field is mandatory' :
  //     this.account.controls.email.hasError('pattern') ? 'Email is Invalid' :
  //       '';
  // }
  genders() {
    if (this.completeform.get('gender').value == 'male')
      return this.male1 = true
    if (this.completeform.get('gender').value == 'female')
      return this.female = true
    else (this.completeform.get('gender').value == 'female')
    return this.others = true
  }
  maritalmethod() {
    if (this.completeform.get('gender').value == 'others') {
      this.completeform.get('maritalstatus').setValue('');
      this.completeform.get('maritalstatus').clearValidators()
    }
    else this.completeform.get('maritalstatus').setValidators([Validators.required])
    this.completeform.get('maritalstatus').updateValueAndValidity()
  }
  submit(): any {
    this.highlightedText.manojInput = "King Kohli"
    console.log(this.completeform)
    this.completeform.get('gender').markAsDirty()
    if (this.completeform.valid)
      console.log('success')
  }
  kingkohli(data: any) {
    console.log(data);
    this.parentInput = data;
  }
}


